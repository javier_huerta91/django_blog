## Nuevo Proyecto - Django Blog
> De acuerdo a lo visto en la capacitacion del día Lunes 29 de Septiembre, dejo el proyecto para que pueda ser estudiado y replicado, de ser posible.

### Recordar
> Algunas consideraciones para que el proyecto pueda quedar andando en su maquina local.

* Instalar y configurar un virtualenv para el proyecto.
* Activar el virtualenv e instalar el archivo de requerimientos del proyecto.
> En la raiz del proyecto debe haber un archivo de requerimientos llamado "requirements.txt", esos requerimientos son paquetes python que se pueden instalar con el gestor de paquetes pip.

>  __"pip install -r requirements.txt"__

* Correr el servidor de prueba
> En la raiz del proyecto, __"python manage.py runserver"__

### Editores de texto utilizados
* [Sublime Text](http://www.sublimetext.com/)
* [Pycharm](http://www.jetbrains.com/pycharm/)

### Paquetes y herramientas utilizadas en el proyecto
* [Django](https://www.djangoproject.com/), Framework en cuestion.
* [South](http://south.readthedocs.org/en/latest/), Manejar migraciones de modelos en aplicaciones.
* [Bootstrap](http://getbootstrap.com/), Framework HTML-CSS para confeccionar vistas de usuario.
* [DjangoActiveLink](https://github.com/j4mie/django-activelink), Hacer parecer que los links presionados se activen de acuerdo a una url.

