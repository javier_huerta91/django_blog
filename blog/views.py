from django.shortcuts import render, get_object_or_404

from blog.models import Post

def posts(request):
    context = {}
    context['object_list'] = Post.objects.order_by('-created_at')
    return render(request, 'blog/posts.html', context)

def post_detail(request, pk):
    context = {}
    post = get_object_or_404(Post, id=pk)
    context['object_list'] = Post.objects.order_by('-created_at')
    context['post'] = post
    return render(request, 'blog/post_detail.html', context)

def about(request):
    return render(request, 'blog/about.html')