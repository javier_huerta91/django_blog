from django.conf.urls import patterns, include, url

urlpatterns = patterns('blog.views',
    url('posts/$', 'posts', name='posts'),
    url(r'post/(?P<pk>[\w\-]+)/$', 'post_detail', name='post_detail'),
    url('about/$', 'about', name='about'),

)

